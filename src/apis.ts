import { Router, Response, Request } from "express";
import express from "express";
import jwt from 'jsonwebtoken';
import NodeCache from "node-cache";
import * as bodyParser from "body-parser"
import { StringifyOptions } from "querystring";

const app = express();
var router = express.Router();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
const nodeCache = new NodeCache();
const privateKey = "(&)&MBMBG&**)*)";


var id = 0;
var users: { id: number, email: string, password: string }[] = [];
// type users = {};

// user registeration
app.post('/register', function(req, res){
    
    const { email, password } = req.body;

    id = id +1;
    var user = {id:id, email: email, password: password, jwToken:'' };
    
    
    var storedUsers = <Array<any>>nodeCache.get("users");
    if(storedUsers){
        users[email] = user;
        if(storedUsers[email]){
            res.status(422);
            res.json("User already exists");
            
        }

        nodeCache.set( "users", users);
    }
    else{
        users[email] = user;
        nodeCache.set( "users", users);
    }
 
    res.json({user:{id:user.id, email:user.email}});
});

// user login
app.post('/login', (req, res) => {

    var storedUsers = <Array<any>>nodeCache.get("users");
    const { email } = req.body;

    if(storedUsers[email]){
        const payload = {
            email:email
        };
        const token = jwt.sign(payload, privateKey, {expiresIn: '30d'});

        storedUsers[email].jwToken = token;
        nodeCache.set( "users", storedUsers);

        res.json({jwt: token});
    }
    else{
        res.status(404);
        res.json("User not found!");
    }
});


// get user
app.get('/user', (req, res) => {

    var storedUsers = <Array<any>>nodeCache.get("users");
    const jwToken = req.header('authorization');

    if(storedUsers && jwToken){
        var jwTokenSplit = jwToken.split(" ");
        for (var index in storedUsers) {      
            if(storedUsers[index].jwToken && storedUsers[index].jwToken == jwTokenSplit[1]){   
                res.json({user:{id:storedUsers[index].id, email:index}});
            }
          }
    }
    else{
        res.status(404);
        res.json("User not found!");
    }

});

app.post('/create-task', (req, res) => {
    res.send('Hello from express and typescript');
});

app.get('/list-tasks', (req, res) => {
    res.send('Hello from express and typescript');
});



const port = process.env.PORT || 3000;

app.listen(port, () => console.log(`App listening on PORT ${port}`));
